package com.development.phoenics.plank30days.widgets

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import com.development.phoenics.plank30days.R


class RotatingCircleView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    companion object {
        private const val DEFAULT_BORDER_COLOR = Color.WHITE
        private const val FIRST_BORDER_WIDTH = 13.0f
        private const val SECOND_BORDER_WIDTH = 20.0f
        private const val RECT_ROTATING_ANGLE = 30
    }

    private var borderColor = DEFAULT_BORDER_COLOR
    private var borderWidth = FIRST_BORDER_WIDTH

    private val firstCirclePaint = Paint()
    private val secondCirclePaint = Paint()
    private val thirdCirclePaint = Paint()
    private val rectanglePaint = Paint()
    private var size = 0
    private var halfSize = 0f


    init {
        firstCirclePaint.isAntiAlias = true
        secondCirclePaint.isAntiAlias = true
        thirdCirclePaint.isAntiAlias = true
        rectanglePaint.isAntiAlias = true
        setupAttributes(attrs)
    }

    private fun setupAttributes(attrs: AttributeSet?) {
        val typedArray = context.theme.obtainStyledAttributes(
            attrs, R.styleable.RotatingCircleView,
            0, 0
        )

        borderColor = typedArray.getColor(
            R.styleable.RotatingCircleView_borderColor,
            DEFAULT_BORDER_COLOR
        )
        borderWidth = typedArray.getDimension(
            R.styleable.RotatingCircleView_borderWidth,
            FIRST_BORDER_WIDTH
        )

        typedArray.recycle()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        drawFirstCircle(canvas)
        drawSecondCircle(canvas)
        drawThirdCircle(canvas)
        drawRectangle(canvas)

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        size = Math.min(measuredWidth, measuredHeight)
        halfSize = size / 2f
        setMeasuredDimension(size, size)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        startAnimation()
    }

    private fun drawFirstCircle(canvas: Canvas) {
        val radius = halfSize

        firstCirclePaint.color = borderColor
        firstCirclePaint.style = Paint.Style.STROKE
        firstCirclePaint.strokeWidth = borderWidth

        canvas.drawCircle(halfSize, halfSize, radius - borderWidth / 2f, firstCirclePaint)
    }

    private fun drawSecondCircle(canvas: Canvas) {
        secondCirclePaint.color = borderColor
        secondCirclePaint.style = Paint.Style.STROKE
        secondCirclePaint.strokeWidth = SECOND_BORDER_WIDTH

        val radius = size / 2.2f
        canvas.drawCircle(halfSize, halfSize, radius - 50f / 2f, secondCirclePaint)
    }

    private fun drawThirdCircle(canvas: Canvas) {
        val radius = size / 4f
        thirdCirclePaint.color = borderColor
        thirdCirclePaint.style = Paint.Style.FILL

        canvas.drawCircle(halfSize, halfSize, radius, thirdCirclePaint)
    }

    private fun drawRectangle(canvas: Canvas) {
        rectanglePaint.color = borderColor
        var angle = 0

        for (i in 0..12) {
            canvas.save()
            canvas.rotate(angle.toFloat(), halfSize, halfSize)
            canvas.drawRect((halfSize) - 5f, (size / 4) - 45f, (halfSize) + 5f, (size / 4) - 20f, rectanglePaint)
            canvas.restore()
            angle += RECT_ROTATING_ANGLE
        }

    }

    private fun startAnimation() {
        clearAnimation()
        val animator = AnimationUtils.loadAnimation(context, R.anim.rotation)
        startAnimation(animator)
    }
}