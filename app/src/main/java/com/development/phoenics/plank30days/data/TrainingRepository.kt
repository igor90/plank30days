package com.development.phoenics.plank30days.data

import android.app.Application
import android.arch.lifecycle.LiveData
import android.content.Context
import com.development.phoenics.plank30days.FIRST_TRAINING
import com.development.phoenics.plank30days.ONE_DAY
import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.TRAINING_DAYS
import com.development.phoenics.plank30days.data.database.TrainingDao
import com.development.phoenics.plank30days.data.database.TrainingRoomDatabase
import com.development.phoenics.plank30days.data.database.models.Training
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*
import java.util.concurrent.TimeUnit

class TrainingRepository(application: Application) {

    private var trainingDao: TrainingDao = TrainingRoomDatabase.getInstance(application)?.trainingDao()!!

    fun getRemainingTrainings(): LiveData<Int> {
        return trainingDao.getRemainingTrainingCount()
    }

    suspend fun getNextSession(context: Context): String {
        return withContext(Dispatchers.IO) {

            val lastFinishedTrainingId = trainingDao.getFinishedTrainings()
            val todaysTraining = context.getString(R.string.todays_training)
            val tomorrowsTraining = context.getString(R.string.tomorrows_training)

            if (lastFinishedTrainingId == 0) {
                return@withContext todaysTraining
            }
            val lastFinishedTraining = trainingDao.getTraining(lastFinishedTrainingId.toLong())

            val currentDate = Date(System.currentTimeMillis())
            val lastFinishedTrainingDate = lastFinishedTraining.currentDay?.let { Date(it) }
            val nextTrainingToAccomplish = lastFinishedTrainingDate?.let { addDay(it, ONE_DAY) }

            if (currentDate.after(nextTrainingToAccomplish)) {
                return@withContext todaysTraining
            }

            return@withContext tomorrowsTraining
        }
    }

    suspend fun accomplishSession(id: Long, executionTime: Long) {
        withContext(Dispatchers.IO) {
            trainingDao.accomplishSession(id)
            makeNextTrainingAvailableToPerform()
            updateTrainingExecutionTime(executionTime)
        }
    }

    suspend fun resetTrainings() {
        withContext(Dispatchers.IO) {
            val currentDateInMillis = Calendar.getInstance().time.time
            val dayInMillis = TimeUnit.DAYS.toMillis(ONE_DAY.toLong())

            for (i in 0 until TRAINING_DAYS) {
                trainingDao.resetTrainings(currentDateInMillis + (i * dayInMillis), (i + ONE_DAY).toLong())
            }
            trainingDao.makeNextTrainingAvailableToPerform(FIRST_TRAINING)
        }
    }

    fun getTrainingsList(): LiveData<List<Training>> {
        return trainingDao.getAllTrainings()
    }

    suspend fun getTraining(id: Long): Training {
        return withContext(Dispatchers.IO) {
            trainingDao.getTraining(id)
        }
    }

    suspend fun updateUnfinishedTrainings() {
        withContext(Dispatchers.IO) {
            val lastFinishedTrainingId = trainingDao.getFinishedTrainings()

            if (lastFinishedTrainingId == 0) {
                return@withContext
            }

            val currentDate = Date(System.currentTimeMillis())
            val currentDateInMillis = Calendar.getInstance().time.time
            val dayInMillis = TimeUnit.DAYS.toMillis(ONE_DAY.toLong())

            val lastFinishedTraining = trainingDao.getTraining(lastFinishedTrainingId.toLong())
            val lastFinishedTrainingDate = lastFinishedTraining.currentDay?.let { Date(it) }
            val nextTrainingToAccomplish = lastFinishedTrainingDate?.let { addDay(it, ONE_DAY) }

            if (currentDate.after(nextTrainingToAccomplish)) {
                for ((j, i) in (lastFinishedTrainingId + ONE_DAY..TRAINING_DAYS).withIndex()) {
                    trainingDao.updateUnfinishedTrainings(currentDateInMillis + (j * dayInMillis), i.toLong())
                }
            } else {
                for ((j, i) in (lastFinishedTrainingId..TRAINING_DAYS).withIndex()) {
                    trainingDao.updateUnfinishedTrainings(currentDateInMillis + (j * dayInMillis), i.toLong())
                }
            }
        }
    }

    private fun updateTrainingExecutionTime(executionTime: Long) {
        val lastFinishedTrainingId = trainingDao.getFinishedTrainings()
        val nextTrainingToAccomplishId = lastFinishedTrainingId + ONE_DAY
        trainingDao.updateNextTrainingExecutionTime(executionTime, nextTrainingToAccomplishId.toLong())
    }

    private fun makeNextTrainingAvailableToPerform() {
        val lastFinishedTrainingId = trainingDao.getFinishedTrainings()
        val availableTrainingId = lastFinishedTrainingId + ONE_DAY
        trainingDao.makeNextTrainingAvailableToPerform(availableTrainingId.toLong())
    }

    private fun addDay(date: Date, numberOfDays: Int): Date {
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.DAY_OF_YEAR, numberOfDays)
        return calendar.time
    }
}