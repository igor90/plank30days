package com.development.phoenics.plank30days.training

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.development.phoenics.plank30days.data.TrainingRepository
import com.development.phoenics.plank30days.data.database.models.Training
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class TrainingsViewModel(application: Application) : AndroidViewModel(application) {

    private val trainingRepository: TrainingRepository = TrainingRepository(application)
    private val mainJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + mainJob)

    fun getTrainingsList() : LiveData<List<Training>> {
        return trainingRepository.getTrainingsList()
    }

    fun resetTrainings() {
        coroutineScope.launch {
            trainingRepository.resetTrainings()
        }
    }

    fun updateUnfinishedTrainings() {
        coroutineScope.launch {
            trainingRepository.updateUnfinishedTrainings()
        }
    }
}
