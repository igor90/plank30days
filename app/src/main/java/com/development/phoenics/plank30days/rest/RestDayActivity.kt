package com.development.phoenics.plank30days.rest

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.text.style.RelativeSizeSpan
import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.TRAINING_KEY
import com.development.phoenics.plank30days.data.database.models.Training
import com.development.phoenics.plank30days.extentions.NEWLINE
import kotlinx.android.synthetic.main.activity_rest.*


class RestDayActivity : AppCompatActivity() {

    private lateinit var viewModel: RestDayViewModel
    private lateinit var training: Training
    private var nextTrainingExecutionTime: Long = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rest)

        title = resources.getString(R.string.rest_day_title)

        viewModel = ViewModelProviders.of(this).get(RestDayViewModel::class.java)
        viewModel.getTraining(intent.getSerializableExtra(TRAINING_KEY) as Long)
        viewModel.currentTraining.observe(this, Observer {
            if (it != null) {
                training = it
                nextTrainingExecutionTime = training.executionTime
            }
        })
        viewModel.finishedTraining.observe(this, Observer { finished ->
            finished?.let {
                if (it) {
                    finish()
                }
            }
        })

        finishBtn.setOnClickListener {
            training.id?.let { training -> viewModel.finishTraining(training, nextTrainingExecutionTime) }
        }
        setRestDaySpanText()
    }

    private fun setRestDaySpanText() {
        val todayText = resources.getString(R.string.today_text)
        val restText = resources.getString(R.string.rest_text)
        val fullText = TextUtils.join(String.NEWLINE, arrayOf(todayText, restText))
        val spannableStringBuilder = SpannableString(fullText)
        spannableStringBuilder.setSpan(RelativeSizeSpan(1.5f), todayText.length, fullText.length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE) // set size
        restDayText.text = spannableStringBuilder
    }
}
