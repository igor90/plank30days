package com.development.phoenics.plank30days.exercise


import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.view.View
import be.rijckaert.tim.animatedvector.FloatingMusicActionButton
import com.development.phoenics.plank30days.ONE_SECOND_IN_MILLIS
import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.TIME_BEFORE_STARTING_TRAINING
import com.development.phoenics.plank30days.TRAINING_KEY
import com.development.phoenics.plank30days.data.database.models.Training
import com.development.phoenics.plank30days.extentions.msTimeFormatter
import kotlinx.android.synthetic.main.activity_exercise.*


class ExerciseActivity : AppCompatActivity() {

    private lateinit var viewModel: ExerciseViewModel
    private lateinit var training: Training

    enum class TimerState {
        Paused, Running
    }

    private lateinit var timer: CountDownTimer
    private var timerState = TimerState.Paused

    private var secondsRemaining: Long = 0
    private var nextTrainingExecutionTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise)
        title = resources.getString(R.string.training_title)

        viewModel = ViewModelProviders.of(this).get(ExerciseViewModel::class.java)
        viewModel.currentTraining.observe(this, Observer {
            if (it != null) {
                training = it
                secondsRemaining = training.executionTime
                nextTrainingExecutionTime = training.executionTime + 5
                countdownText.text = (training.executionTime * ONE_SECOND_IN_MILLIS).msTimeFormatter()
                setProgressBarValues(training.executionTime.toInt())
            }
        })
        viewModel.finishedTraining.observe(this, Observer { finished ->
            finished?.let {
                if (it) {
                    finish()
                }
            }
        })
        viewModel.getTraining(intent.getSerializableExtra(TRAINING_KEY) as Long)

        setButtonsClickListeners()

        playPauseFab.setImageDrawable(resources.getDrawable(R.drawable.play_icon, null))
    }

    override fun onPause() {
        super.onPause()
        if (timerState == TimerState.Running) {
            playPauseFab.playAnimation()
            stopCountDownTimer()
        }
    }

    private fun startStopTimer() {
        if (timerState == TimerState.Paused) {
            startCountDownTimer()
        } else {
            stopCountDownTimer()
        }
    }

    private fun startCountDownTimer() {
        timerState = TimerState.Running

        timer = object : CountDownTimer(secondsRemaining * ONE_SECOND_IN_MILLIS, ONE_SECOND_IN_MILLIS.toLong() / 2) {
            override fun onFinish() = onTimerFinished()

            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished / ONE_SECOND_IN_MILLIS
                updateCountdownUI(millisUntilFinished)
            }
        }.start()
    }

    @SuppressLint("RestrictedApi")
    private fun onTimerFinished() {
        timerState = TimerState.Paused
        playPauseFab.playAnimation()

        saveTrainingBtn.visibility = View.VISIBLE
        playPauseFab.visibility = View.GONE

        saveTrainingBtn.startAnimation()
    }

    private fun stopCountDownTimer() {
        timerState = TimerState.Paused
        timer.cancel()
    }

    private fun updateCountdownUI(milliSeconds: Long) {
        countdownText.text = milliSeconds.msTimeFormatter()
        progressBarCircle.progress = (secondsRemaining).toInt()
    }

    private fun setProgressBarValues(timerLengthSeconds: Int) {
        progressBarCircle.max = timerLengthSeconds
        progressBarCircle.progress = timerLengthSeconds
    }

    private fun setButtonsClickListeners() {
        saveTrainingBtn.setOnClickListener {
            training.id?.let { training -> viewModel.finishTraining(training, nextTrainingExecutionTime) }
        }

        playPauseFab.setOnMusicFabClickListener(object : FloatingMusicActionButton.OnMusicFabClickListener {
            override fun onClick(view: View) {
                showStartingTextTimer()
                startingTrainingText.visibility = View.VISIBLE
            }
        })
    }

    private fun showStartingTextTimer() {
        playPauseFab.isEnabled = false
        object : CountDownTimer(TIME_BEFORE_STARTING_TRAINING * ONE_SECOND_IN_MILLIS, ONE_SECOND_IN_MILLIS.toLong() / 2) {
            override fun onFinish() {
                startingTrainingText.visibility = View.GONE
                startStopTimer()
                playPauseFab.isEnabled = true
                playPauseFab.setOnMusicFabClickListener(object : FloatingMusicActionButton.OnMusicFabClickListener {
                    override fun onClick(view: View) {
                        startStopTimer()
                    }
                })
            }

            override fun onTick(millisUntilFinished: Long) {
                startingTrainingText.text = resources.getString(R.string.training_starts_in, millisUntilFinished / ONE_SECOND_IN_MILLIS)
            }
        }.start()
    }
}
