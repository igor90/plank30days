package com.development.phoenics.plank30days.widgets

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.os.Build
import android.util.AttributeSet
import android.view.View
import com.development.phoenics.plank30days.R

/**
 * @author Igor Cocu
 */
class CircularProgressView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private var size = 0
    private var halfSize = 0f
    private val paint = Paint().apply {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            color = context?.resources?.getColor(R.color.mint, null)!!
        }
        style = Paint.Style.FILL
    }

    private var percentage = 0

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        size = Math.min(measuredWidth, measuredHeight)
        halfSize = size / 2f
        setMeasuredDimension(size, size)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.let {
            drawProgress(it, percentage)
        }
    }

    fun setPercentage(percentage: Int) {
        this.percentage = percentage
        invalidate()
    }

    private fun drawProgress(canvas: Canvas, percentage: Int) {
        val fillPercentage = (360 * (percentage / 100.0)).toFloat()
        val rectF = RectF()
        rectF.apply {
            set(0f, 0f, size.toFloat(), size.toFloat())
        }
        canvas.drawArc(rectF, 270f, fillPercentage, true, paint)
    }
}