package com.development.phoenics.plank30days.training.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.data.database.models.Training
import android.support.v7.util.DiffUtil

class TrainingRecycleViewAdapter : RecyclerView.Adapter<ViewHolderTrainingItem>() {

    private var trainingList: MutableList<Training> = mutableListOf()
    var onTrainingItemClickListener: OnTrainingItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderTrainingItem {
        val inflater = LayoutInflater.from(parent.context)
        val trainingItem = inflater.inflate(R.layout.item_training_cell, parent, false)
        return ViewHolderTrainingItem(trainingItem, onTrainingItemClickListener)
    }

    override fun getItemCount(): Int {
        return this.trainingList.size
    }

    override fun onBindViewHolder(holder: ViewHolderTrainingItem, position: Int) {
        holder.bind(trainingList[position])
    }


    fun addAll(list: List<Training>) {
        trainingList.clear()
        trainingList.addAll(list)
        notifyDataSetChanged()
    }

    interface OnTrainingItemClickListener {
        fun onTrainingSelected(training: Training)
        fun onRestDaySelected(training: Training)
    }
}