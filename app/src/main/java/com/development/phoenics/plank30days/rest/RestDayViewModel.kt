package com.development.phoenics.plank30days.rest

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.development.phoenics.plank30days.data.TrainingRepository
import com.development.phoenics.plank30days.data.database.models.Training
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class RestDayViewModel(application: Application) : AndroidViewModel(application) {

    private val trainingRepository: TrainingRepository = TrainingRepository(application)
    private val _currentTraining = MutableLiveData<Training>()
    val finishedTraining = MutableLiveData<Boolean>()

    private val mainJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + mainJob)

    val currentTraining: LiveData<Training>
        get() = _currentTraining

    fun getTraining(id: Long) {
        coroutineScope.launch {
            val training = trainingRepository.getTraining(id)
            _currentTraining.postValue(training)
        }
    }

    fun finishTraining(id: Long, executionTime: Long) {
        coroutineScope.launch {
            trainingRepository.accomplishSession(id, executionTime)
            finishedTraining.postValue(true)
        }
    }
}