package com.development.phoenics.plank30days.extentions

import com.development.phoenics.plank30days.TIMER_STRING_FORMAT
import java.util.concurrent.TimeUnit

fun Long.msTimeFormatter(): String {
    return String.format(
        TIMER_STRING_FORMAT,
        TimeUnit.MILLISECONDS.toMinutes(this) - TimeUnit.HOURS.toMinutes(
            TimeUnit.MILLISECONDS.toHours(this)
        ),
        TimeUnit.MILLISECONDS.toSeconds(this) - TimeUnit.MINUTES.toSeconds(
            TimeUnit.MILLISECONDS.toMinutes(this)
        )
    )
}