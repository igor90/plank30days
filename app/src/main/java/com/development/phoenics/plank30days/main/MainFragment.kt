package com.development.phoenics.plank30days.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.development.phoenics.plank30days.FRAGMENT_KEY
import com.development.phoenics.plank30days.MAIN_FRAGMENT_STACK_LEVEL

import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.TRAINING_DAYS
import com.development.phoenics.plank30days.training.TrainingsFragment
import com.development.phoenics.plank30days.widgets.ItemViewAlphaTouch
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext


class MainFragment : Fragment(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.getRemainingTrainingDays().observe(this, Observer { trainingsCount ->
            remainedTrainingDaysText.text = resources.getString(R.string.days_left, trainingsCount.toString())
            trainingsCount?.let {
                val progress = TRAINING_DAYS - it
                circularView.setPercentage(progress * 100 / TRAINING_DAYS)
            }
        })

        viewModel.nextSession.observe(this, Observer { nextSession ->
            nextSessionText.text = resources.getString(R.string.next_session, nextSession)
        })
        context?.let { viewModel.getNextSession(it) }
        circleViewLayout.setOnClickListener {
            openTrainingsFragment()
        }
        circleViewLayout.setOnTouchListener(ItemViewAlphaTouch())
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(FRAGMENT_KEY, MAIN_FRAGMENT_STACK_LEVEL)
    }

    private fun openTrainingsFragment() {
        val trainingsFragment = TrainingsFragment.newInstance(circleViewLayout)
        fragmentManager
            ?.beginTransaction()
            ?.setCustomAnimations(R.animator.fragment_slide_top_enter,
            R.animator.fragment_slide_top_exit,
            R.animator.fragment_slide_down_enter,
            R.animator.fragment_slide_down_exit)
            ?.replace(R.id.container, trainingsFragment)
            ?.addToBackStack(null)
            ?.commit()
    }
}
