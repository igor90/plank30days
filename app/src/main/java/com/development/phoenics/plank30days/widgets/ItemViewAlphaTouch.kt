package com.development.phoenics.plank30days.widgets

import android.view.MotionEvent
import android.view.View

class ItemViewAlphaTouch (var itemPressed: Float = 0.3f, var itemNormal: Float = 1f) : View.OnTouchListener {

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> v?.alpha = itemPressed
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> v?.alpha = itemNormal
        }
        return false
    }
}