package com.development.phoenics.plank30days.training.adapter

import android.support.v7.util.DiffUtil
import com.development.phoenics.plank30days.data.database.models.Training

class TrainingDiffUtilCallBack(private val oldTrainingsList: List<Training>, private val newTrainingsList: List<Training>) :
    DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldTrainingsList[oldItemPosition].id == newTrainingsList[newItemPosition].id
    }

    override fun getOldListSize(): Int {
        return oldTrainingsList.size
    }

    override fun getNewListSize(): Int {
        return newTrainingsList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldTrainingsList[oldItemPosition] == newTrainingsList[newItemPosition]
    }
}