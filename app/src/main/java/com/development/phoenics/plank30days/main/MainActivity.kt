package com.development.phoenics.plank30days.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.development.phoenics.plank30days.FRAGMENT_KEY
import com.development.phoenics.plank30days.MAIN_FRAGMENT_STACK_LEVEL
import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.TRAINING_FRAGMENT_STACK_LEVEL
import com.development.phoenics.plank30days.training.TrainingsFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            addMainFragment()
        } else {
            when {
                savedInstanceState.getInt(FRAGMENT_KEY) == MAIN_FRAGMENT_STACK_LEVEL -> addMainFragment()
                savedInstanceState.getInt(FRAGMENT_KEY) == TRAINING_FRAGMENT_STACK_LEVEL -> addTrainingsFragment()
            }
        }
    }

    private fun addMainFragment() {
        val fragmentManager = supportFragmentManager
        val mainFragment = MainFragment.newInstance()
        fragmentManager.beginTransaction().add(R.id.container, mainFragment).commit()
    }

    private fun addTrainingsFragment() {
        val fragmentManager = supportFragmentManager
        val trainingsFragment = TrainingsFragment.newInstance()
        fragmentManager.beginTransaction().add(R.id.container, trainingsFragment).commit()
    }
}
