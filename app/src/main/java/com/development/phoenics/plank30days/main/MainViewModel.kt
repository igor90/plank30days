package com.development.phoenics.plank30days.main

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.development.phoenics.plank30days.data.TrainingRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val trainingRepository: TrainingRepository = TrainingRepository(application)
    private val _nextSession = MutableLiveData<String>()

    private val mainJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + mainJob)


    val nextSession: LiveData<String>
        get() = _nextSession


    fun getRemainingTrainingDays(): LiveData<Int> {
        return trainingRepository.getRemainingTrainings()
    }

    fun getNextSession(context: Context) {
        coroutineScope.launch {
            val nextSession = trainingRepository.getNextSession(context)
            _nextSession.postValue(nextSession)
        }
    }

    override fun onCleared() {
        super.onCleared()
        mainJob.cancel()
    }
}