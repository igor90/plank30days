package com.development.phoenics.plank30days.training.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.development.phoenics.plank30days.EXERCISE_TIMER_FORMAT
import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.data.database.models.Training
import java.text.SimpleDateFormat
import java.util.*

class ViewHolderTrainingItem(view: View, private val listener: TrainingRecycleViewAdapter.OnTrainingItemClickListener?) : RecyclerView.ViewHolder(view) {


    private val trainingDay: TextView? = view.findViewById(R.id.trainingDay)
    private val trainingDate: TextView? = view.findViewById(R.id.sessionDate)
    private var doneImage: ImageView? = view.findViewById(R.id.doneImage)
    private var restImage: ImageView? = view.findViewById(R.id.restImage)
    private val itemLayout: ConstraintLayout? = view.findViewById(R.id.itemLayout)

    private val simpleDateFormat = SimpleDateFormat(EXERCISE_TIMER_FORMAT, Locale.getDefault())

    fun bind(training: Training) {
        val trainingDateFormat = simpleDateFormat.format(training.currentDay)
        trainingDate?.text = trainingDateFormat
        trainingDay?.text = itemView.resources.getString(R.string.training_day, training.id)
        when {
            training.finished -> {
                itemLayout?.background = itemView.resources.getDrawable(R.drawable.finished_training_rectangle, null)
                doneImage?.visibility = View.VISIBLE
                restImage?.visibility = View.GONE
                itemView.setOnClickListener(null)
            }

            training.canPerformTraining -> {
                itemView.isEnabled = true
                if (training.isRestDay) {
                    doneImage?.visibility = View.GONE
                    restImage?.visibility = View.VISIBLE
                    itemView.setOnClickListener {
                        listener?.onRestDaySelected(training)
                        itemView.isEnabled = false
                    }
                } else {
                    doneImage?.visibility = View.GONE
                    restImage?.visibility = View.GONE
                    itemView.setOnClickListener {
                        listener?.onTrainingSelected(training)
                        itemView.isEnabled = false
                    }
                }
                itemLayout?.background = itemView.resources.getDrawable(R.drawable.perform_training_rectangle, null)
                itemLayout?.elevation = itemView.resources.getDimension(R.dimen.training_item_elevation)
            }

            training.isRestDay -> {
                doneImage?.visibility = View.GONE
                restImage?.visibility = View.VISIBLE
                itemLayout?.background = itemView.resources.getDrawable(R.drawable.rounded_rectangle, null)
                itemView.setOnClickListener(null)
            }

            else -> {
                itemLayout?.background = itemView.resources.getDrawable(R.drawable.rounded_rectangle, null)
                doneImage?.visibility = View.GONE
                restImage?.visibility = View.GONE
                itemView.setOnClickListener(null)
            }
        }
    }
}