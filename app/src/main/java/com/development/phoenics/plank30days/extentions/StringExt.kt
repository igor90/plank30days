package com.development.phoenics.plank30days.extentions

val String.Companion.NEWLINE: String get() = "\n"
