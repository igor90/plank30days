package com.development.phoenics.plank30days

const val ONE_DAY = 1
const val TRAINING_DAYS = 30

const val FINISHED_TRAINING = 1
const val UNFINISHED_TRAINING = 0

const val FIRST_TRAINING = 1L

const val AVAILABLE_TRAINING = 1
const val UNAVAILABLE_TRAINING = 0

const val TRAINING_KEY = "training"
const val FRAGMENT_KEY = "main"
const val ONE_SECOND_IN_MILLIS = 1000

const val MAIN_FRAGMENT_STACK_LEVEL = 1
const val TRAINING_FRAGMENT_STACK_LEVEL = 2

const val TIMER_STRING_FORMAT = "%02d:%02d"

const val SAVE_BUTTON_ANIMATION_DURATION = 700L
const val TIME_BEFORE_STARTING_TRAINING = 4L
const val EXERCISE_TIMER_FORMAT = "EEE, d MMM"