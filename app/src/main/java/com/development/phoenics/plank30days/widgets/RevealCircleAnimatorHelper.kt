package com.development.phoenics.plank30days.widgets

import android.animation.Animator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator

class RevealCircleAnimatorHelper {

    companion object {

        const val EXTRAS = "revealExtras"
        const val SOURCE_X = "xSource"
        const val SOURCE_Y = "ySource"
        const val CIRCULAR_SPEED = 1000L

        private var xSource = 0f
        private var ySource = 0f

        fun addBundleValues(arguments: Bundle?, sourceView: View?) {
            if (sourceView != null && arguments != null) {
                return arguments.putBundle(EXTRAS, Bundle().apply {
                    putFloat(SOURCE_X, sourceView.x + sourceView.width / 2)
                    putFloat(SOURCE_Y, sourceView.y + sourceView.height / 2)
                })
            }
        }
    }

    fun create(fragment: Fragment, container: ViewGroup?) {
        fragment.arguments?.getBundle(EXTRAS)?.apply {
            xSource = this.getFloat(SOURCE_X)
            ySource = this.getFloat(SOURCE_Y)
        }
        container?.let {
            if (container.x < xSource) {
                xSource -= container.x
            } else xSource = 0f
            if (container.y < ySource) ySource -= container.y else ySource = 0f
        }
    }

    fun start(rootLayout: View) {
        if (xSource >= 0 && ySource >= 0) {
            rootLayout.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
                override fun onLayoutChange(
                    rootLayout: View?,
                    p1: Int,
                    p2: Int,
                    p3: Int,
                    p4: Int,
                    p5: Int,
                    p6: Int,
                    p7: Int,
                    p8: Int
                ) {
                    rootLayout?.let {
                        getCircularAnimator(it, xSource.toInt(), ySource.toInt(), CIRCULAR_SPEED).start()
                    }
                    rootLayout?.removeOnLayoutChangeListener(this)
                }
            })
        }
    }

    private fun getCircularAnimator(targetView: View, sourceX: Int, sourceY: Int, speed: Long): Animator {
        val finalRadius = Math.hypot(targetView.width.toDouble(), targetView.height.toDouble()).toFloat()
        return ViewAnimationUtils.createCircularReveal(targetView, sourceX, sourceY, 0f, finalRadius).apply {
            interpolator = AccelerateDecelerateInterpolator()
            duration = speed
        }
    }
}