package com.development.phoenics.plank30days.data.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.development.phoenics.plank30days.AVAILABLE_TRAINING
import com.development.phoenics.plank30days.data.database.models.Training
import com.development.phoenics.plank30days.FINISHED_TRAINING
import com.development.phoenics.plank30days.UNAVAILABLE_TRAINING
import com.development.phoenics.plank30days.UNFINISHED_TRAINING

@Dao
interface TrainingDao {

    @Insert
    fun insert(training: Training)

    @Insert
    fun insertAll(training: MutableList<Training>)

    @Query("SELECT * from training")
    fun getAllTrainings(): LiveData<List<Training>>

    @Query("SELECT COUNT(finished) FROM training WHERE finished = $UNFINISHED_TRAINING")
    fun getRemainingTrainingCount(): LiveData<Int>

    @Query("SELECT COUNT(finished) FROM training WHERE finished = $FINISHED_TRAINING")
    fun getFinishedTrainings(): Int

    @Query("SELECT id, currentDay, finished, executionTime, canPerformTraining, isRestDay from training WHERE id = :id")
    fun getTraining(id: Long?): Training

    @Query("UPDATE training SET finished = $FINISHED_TRAINING WHERE id = :id")
    fun accomplishSession(id: Long?)

    @Query("UPDATE training SET currentDay = :currentDay, finished = $UNFINISHED_TRAINING, canPerformTraining = $UNAVAILABLE_TRAINING WHERE id = :id")
    fun resetTrainings(currentDay: Long, id: Long?)

    @Query("UPDATE training SET canPerformTraining = $AVAILABLE_TRAINING WHERE id = :id")
    fun makeNextTrainingAvailableToPerform(id: Long?)

    @Query("UPDATE training SET currentDay = :currentDay WHERE id = :id")
    fun updateUnfinishedTrainings(currentDay: Long, id: Long?)

    @Query("UPDATE training SET executionTime = :executionTime WHERE id = :id")
    fun updateNextTrainingExecutionTime(executionTime: Long, id: Long?)

}