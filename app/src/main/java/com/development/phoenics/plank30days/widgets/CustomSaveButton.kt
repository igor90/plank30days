package com.development.phoenics.plank30days.widgets

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.widget.Button
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.animation.ObjectAnimator
import android.util.TypedValue
import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.SAVE_BUTTON_ANIMATION_DURATION


class CustomSaveButton(context: Context?, attrs: AttributeSet?) : Button(context, attrs) {

    private var mGradientDrawable = ContextCompat.getDrawable(context!!, R.drawable.shape_default) as GradientDrawable
    private var mIsMorphingInProgress: Boolean = false

    init {
        background = mGradientDrawable
    }

    fun startAnimation() {
        val initialWidth = resources.getDimension(R.dimen.custom_save_btn_initial_width).toInt()
        val initialHeight = resources.getDimension(R.dimen.custom_save_btn_initial_width).toInt()

        val initialCornerRadius = resources.getDimension(R.dimen.custom_save_btn_width)
        val finalCornerRadius = resources.getDimension(R.dimen.custom_save_btn_final_radius)
        mIsMorphingInProgress = true
        this.text = null
        isClickable = false

        val toWidth = resources.getDimension(R.dimen.custom_save_btn_width).toInt()

        val cornerAnimation = ObjectAnimator.ofFloat(
            mGradientDrawable,
            "cornerRadius",
            initialCornerRadius,
            finalCornerRadius
        )

        val widthAnimation = ValueAnimator.ofInt(initialWidth, toWidth)
        widthAnimation.addUpdateListener { valueAnimator ->
            val animatedWidth = valueAnimator.animatedValue as Int
            val layoutParams = layoutParams
            layoutParams.width = animatedWidth
            setLayoutParams(layoutParams)
        }

        val heightAnimation = ValueAnimator.ofInt(initialHeight, initialHeight)
        heightAnimation.addUpdateListener { valueAnimator ->
            val animatedHeight = valueAnimator.animatedValue as Int
            val layoutParams = layoutParams
            layoutParams.height = animatedHeight
            setLayoutParams(layoutParams)
        }

        val mMorphingAnimatorSet = AnimatorSet()
        mMorphingAnimatorSet.duration = SAVE_BUTTON_ANIMATION_DURATION
        mMorphingAnimatorSet.playTogether(cornerAnimation, widthAnimation, heightAnimation)
        mMorphingAnimatorSet.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                mIsMorphingInProgress = false
                setSaveButtonOnAnimationEnd()
            }
        })
        mMorphingAnimatorSet.start()
    }

    private fun setSaveButtonOnAnimationEnd() {
        this@CustomSaveButton.isClickable = true
        this@CustomSaveButton.background = resources.getDrawable(R.drawable.save_button_selector, null)
        this@CustomSaveButton.text = resources.getString(R.string.save)
        this@CustomSaveButton.setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            resources.getDimensionPixelSize(R.dimen.save_btn_text_size).toFloat()
        )
    }
}