package com.development.phoenics.plank30days.training

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.development.phoenics.plank30days.FRAGMENT_KEY
import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.TRAINING_FRAGMENT_STACK_LEVEL
import com.development.phoenics.plank30days.TRAINING_KEY
import com.development.phoenics.plank30days.data.database.models.Training
import com.development.phoenics.plank30days.exercise.ExerciseActivity
import com.development.phoenics.plank30days.rest.RestDayActivity
import com.development.phoenics.plank30days.training.adapter.TrainingRecycleViewAdapter
import com.development.phoenics.plank30days.widgets.RevealCircleAnimatorHelper
import kotlinx.android.synthetic.main.training_fragment.*

class TrainingsFragment : Fragment(), TrainingRecycleViewAdapter.OnTrainingItemClickListener {

    companion object {
        fun newInstance(sourceView: View? = null) = TrainingsFragment().apply {
            arguments = Bundle()
            RevealCircleAnimatorHelper.addBundleValues(arguments, sourceView)
        }
    }

    private lateinit var viewModel: TrainingsViewModel
    private lateinit var viewAdapter: TrainingRecycleViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        val root = inflater.inflate(R.layout.training_fragment, container, false)
        fragmentManager?.addOnBackStackChangedListener {
            RevealCircleAnimatorHelper().apply {
                create(this@TrainingsFragment, container)
                start(root)
            }
        }
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TrainingsViewModel::class.java)
        viewModel.getTrainingsList().observe(this, Observer { trainings ->
            viewAdapter.addAll(trainings as MutableList<Training>)
        })
        setupRecycler()
        viewAdapter.onTrainingItemClickListener = this
    }

    override fun onResume() {
        super.onResume()
        viewModel.updateUnfinishedTrainings()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(FRAGMENT_KEY, TRAINING_FRAGMENT_STACK_LEVEL)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.trainings_menu, menu)

        val resetMenuItem = menu?.findItem(R.id.resetTrainings)
        resetMenuItem?.setOnMenuItemClickListener {
            showResetTrainingsAlertDialog()
            false
        }
    }

    private fun setupRecycler() {
        viewAdapter = TrainingRecycleViewAdapter()
        val viewManager = LinearLayoutManager(context)

        trainingRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    override fun onTrainingSelected(training: Training) {
        val intent = Intent(activity, ExerciseActivity::class.java)
        intent.putExtra(TRAINING_KEY, training.id)
        startActivity(intent)
    }

    override fun onRestDaySelected(training: Training) {
        val intent = Intent(activity, RestDayActivity::class.java)
        intent.putExtra(TRAINING_KEY, training.id)
        startActivity(intent)
    }

    private fun showResetTrainingsAlertDialog() {
        val builder = AlertDialog.Builder(context)

        builder.setTitle(R.string.reset_trainings_title)
            .setMessage(R.string.reset_trainings_message)
            .setPositiveButton(
                R.string.reset
            ) { dialog, _ ->
                dialog.dismiss()
                viewModel.resetTrainings()
            }
            .setNegativeButton(
                R.string.cancel
            ) { dialog, _ ->
                dialog.dismiss()
            }
        builder.create().show()
    }


}
